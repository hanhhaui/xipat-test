import { Button, Grid, TextField } from "@mui/material";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { Box } from "@mui/system";
import { MuiColorInput, MuiColorInputColors } from "mui-color-input";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { DateRangePicker } from "@mui/x-date-pickers-pro/DateRangePicker";
import { SingleInputDateRangeField } from "@mui/x-date-pickers-pro/SingleInputDateRangeField";
import { DateRange } from "@mui/x-date-pickers-pro";
import LabelIcon from '@mui/icons-material/Label';

type Props = {};

type FormData = {
  title: string;
  email: string;
  color: string;
  dateRange:DateRange<unknown>;
};

export const Settings: React.FC<Props> = () => {
  const [value, setValue] = React.useState("#1976d2");

  const handleChange = (value: string, colors: MuiColorInputColors) => {
    setValue(value);
  };

  const EMAIL_REGEX = /^\S+@\S+\.\S+$/;

  const methods = useForm<FormData>({
    mode: "onBlur",
    defaultValues: {
      email: "",
      title: "",
      color: "",
    },
  });

  const {
    register,
    handleSubmit,
    formState: { errors, isDirty },
    control
  } = methods;

  const onSubmit = handleSubmit(({ title, email,dateRange }) => {
    console.log("title", title);
    console.log("email", email);
    console.log("color", value);
    console.log("dateRange", dateRange);
  });

  return (
    <><Box display='flex' alignItems='center' gap={1}><LabelIcon color="disabled"/>Setting</Box><form onSubmit={onSubmit}>
    <Grid container spacing={2} mt={1}>
      <Grid item xs={12} container spacing={2}>
        <Grid item xs={3}>
          <TextField
            {...register("title", { required: true })}
            inputProps={{ maxLength: 255 }}
            label="Title"
            required
            name="title"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            fullWidth
            error={!!errors.title}
            helperText={errors.title ? "Invalid title" : ""}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            {...register("email", {
              required: true,
              pattern: {
                value: EMAIL_REGEX,
                message: "Invalid email address",
              },
            })}
            inputProps={{ maxLength: 255 }}
            label="Email"
            required
            name="email"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            fullWidth
            error={!!errors.email}
            helperText={errors.email ? "Invalid email address" : ""}
          />
        </Grid>
      </Grid>
      <Grid item xs={12} container spacing={2}>
        <Grid item xs={3}>
          <Box display="flex" alignItems="center" gap={2} marginTop={1}>
            <MuiColorInput
              style={{ width: "100%" }}
              value={value}
              label="Color picker"
              onChange={handleChange}
              inputProps={{ style: { color: `${value}` } }}
            />
          </Box>
        </Grid>
        <Grid item xs={3}>
          <Controller 
            name="dateRange" 
            control={control} render={({ field: { onChange, onBlur, value, ref } }) => (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DemoContainer 
          components={["SingleInputDateRangeField"]}>
          <DateRangePicker
            slots={{ field: SingleInputDateRangeField }}
            label="Active date"
            value={value}
            onChange={onChange}
          />
        </DemoContainer>
      </LocalizationProvider>
      )} />
        </Grid>
      </Grid>
      {isDirty && (
        <Grid item xs={6} container justifyContent="flex-end">
          <Button variant="outlined" size="large" onClick={onSubmit}>
            Save
          </Button>
        </Grid>
      )}
    </Grid>
  </form></>
    
  );
};

export default React.memo(Settings);

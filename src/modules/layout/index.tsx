import React from "react";
import { Box, Tab, Tabs } from "@mui/material";
import { useLocation, Link } from "react-router-dom";
import Dashboard from "../dashboard";
import Posts from "../posts";
import Settings from "../settings";

type Props = {};

type Route = {
  path: string;
  element: React.ReactNode;
  label: string;
};

export const routes: Route[] = [
  {
    path: "/dashboard",
    element: <Dashboard />,
    label: "Dashboard",
  },
  {
    path: "/users",
    element: <Posts />,
    label: "Users",
  },
  {
    path: "/settings",
    element: <Settings />,
    label: "Settings",
  },
];

export const Layout: React.FC<Props> = () => {
  const { pathname } = useLocation();

  const a11yProps = (index: number) => {
    return {
      id: `vertical-tab-${index}`,
      "aria-controls": `vertical-tabpanel-${index}`,
    };
  };

  return (
    <Box
      sx={{
        flexGrow: 1,
        bgcolor: "background.paper",
        display: "flex",
        height: 224,
      }}
    >
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={pathname}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: "divider" }}
      >
        {routes?.map((item) => {
          return (
            <Tab
              label={item?.label}
              key={item?.path}
              component={Link}
              value={item?.path}
              to={item?.path}
              {...a11yProps(0)}
            />
          );
        })}
      </Tabs>
    </Box>
  );
};

export default React.memo(Layout);

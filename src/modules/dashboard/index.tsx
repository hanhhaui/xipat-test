import { Button, Grid, Box, styled } from "@mui/material";
import React, { useState } from "react";
import { Supcription } from "./components/Subcription";
import Revenue from "./components/Revenue";
import LabelIcon from "@mui/icons-material/Label";


type Props = {};

const ButtonStyled = styled(Button)({
  marginLeft: "8px",
});

export const Dashboard: React.FC<Props> = () => {
  const [isSubcription, setIsSubcription] = useState(true);
  return (
    <>
      <Box display='flex' alignItems='center' gap={1} mb={1}><LabelIcon color="disabled"/>Dashboard</Box>
      <Button variant="outlined" onClick={() => setIsSubcription(true)}>
        Subcription
      </Button>
      <ButtonStyled onClick={() => setIsSubcription(false)} variant="outlined">
        Revenue
      </ButtonStyled>
      <Grid container>{isSubcription ? <Supcription /> : <Revenue />}</Grid>
    </>
  );
};

export default React.memo(Dashboard);

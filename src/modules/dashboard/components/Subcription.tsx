import React from "react";
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  Tooltip,
  XAxis,
} from "recharts";

type Props = {};

export const Supcription: React.FC<Props> = () => {
  const data = [
    { name: "Mon", Subscribers: 400, amt: 2400 },
    { name: "Tue", Subscribers: 300, amt: 2210 },
    { name: "Wed", Subscribers: 200, amt: 2290 },
    { name: "Thu", Subscribers: 278, amt: 2000 },
    { name: "Fri", Subscribers: 189, amt: 2181 },
    { name: "Sat", Subscribers: 239, amt: 2500 },
    { name: "Sun", Subscribers: 349, amt: 2100 },
  ];
  return (
    <LineChart
      width={1000}
      height={600}
      data={data}
      margin={{ top: 20, right: 20, left: 10, bottom: 5 }}
    >
      <XAxis dataKey="name" />
      <Tooltip />
      <CartesianGrid strokeDasharray="3 3" />
      <Legend />
      <Line
        type="monotone"
        dataKey="Subscribers"
        label="Number of Cat"
        stroke="#ff7300"
        yAxisId={0}
      />
    </LineChart>
  );
};

export default React.memo(Supcription);

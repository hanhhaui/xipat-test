import React from "react";
import { Bar, BarChart, CartesianGrid, Legend, Tooltip, XAxis } from "recharts";

type Props = {};

export const Revenue: React.FC<Props> = () => {
  const data = [
    { name: "Jan", Revenue: 400, amt: 2400 },
    { name: "Feb", Revenue: 300, amt: 2210 },
    { name: "Mar", Revenue: 200, amt: 2290 },
    { name: "Apr", Revenue: 278, amt: 2000 },
    { name: "May", Revenue: 189, amt: 2181 },
    { name: "Jun", Revenue: 239, amt: 2500 },
    { name: "Jul", Revenue: 349, amt: 2100 },
  ];
  return (
    <BarChart
      width={1000}
      height={600}
      data={data}
      margin={{ top: 20, right: 20, left: 10, bottom: 5 }}
    >
      <XAxis dataKey="name" />
      <Tooltip />
      <CartesianGrid strokeDasharray="3 3" />
      <Legend />
      <Bar dataKey="Revenue" label="Number of Cat" yAxisId={0} fill="#8884d8" />
    </BarChart>
  );
};

export default React.memo(Revenue);

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  TableCell,
  TableRow,
  TextField,
  Tooltip,
} from "@mui/material";
import React, { useState } from "react";
import EditIcon from "@mui/icons-material/Edit";
import { User } from "..";

type Props = {
  user: User;
};

export const UserRow: React.FC<Props> = ({ user }) => {
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Dialog open={open} onClose={handleClose} maxWidth="xs">
        <DialogTitle id="alert-dialog-title">User Information</DialogTitle>
        <DialogContent>
          <Grid container spacing={2} mt={0.5}>
            <Grid item xs={12}>
              <TextField
                inputProps={{ maxLength: 255 }}
                label="ID"
                InputLabelProps={{ shrink: true }}
                variant="outlined"
                fullWidth
                value={user?.id}
                size="small"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                inputProps={{ maxLength: 255 }}
                label="UserId"
                InputLabelProps={{ shrink: true }}
                variant="outlined"
                fullWidth
                value={user?.userId}
                size="small"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                inputProps={{ maxLength: 255 }}
                label="Title"
                InputLabelProps={{ shrink: true }}
                variant="outlined"
                fullWidth
                value={user?.title}
                rows={3}
                multiline
                size="small"
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        </DialogActions>
      </Dialog>
      <TableRow
        key={user.id}
        sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
      >
        <TableCell component="th" scope="user">
          {user.id}
        </TableCell>
        <TableCell>{user.userId}</TableCell>
        <TableCell>{user.title}</TableCell>
        <TableCell align="right">
          <Tooltip title="Patient Information">
            <IconButton onClick={() => setOpen(true)} color="primary">
              <EditIcon />
            </IconButton>
          </Tooltip>
        </TableCell>
      </TableRow>
    </>
  );
};

export default React.memo(UserRow);

import {
  Box,
  Grid,
  Paper,
  styled,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import UserRow from "./components/userRow";
import DraftsRoundedIcon from "@mui/icons-material/DraftsRounded";
import LabelIcon from "@mui/icons-material/Label";


type Props = {};

export interface User {
  userId: number;
  id: number;
  title: string;
}

const TextFieldStyled = styled(TextField)({
  marginTop: "16px",
  marginBottom: "16px",
});

export const Posts: React.FC<Props> = () => {
  const [data, setData] = useState<User[]>([]);
  const [page, setPage] = useState(0);
  const [valueFilter, setValueFilter] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const fetchPosts = async () => {
    try {
      let endPointFilter = "";
      if (+valueFilter) {
        endPointFilter = `?userId=${+valueFilter}`;
      }
      const endPoint =
        "https://jsonplaceholder.typicode.com/posts" + endPointFilter;
      const response = await fetch(endPoint);
      const data = await response.json();
      setData(
        (data as User[])?.filter((item) =>
          +valueFilter ? item : item?.title?.includes(valueFilter)
        )
      );
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchPosts();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [valueFilter]);

  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const changeValue = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPage(0);
    setValueFilter(event.target.value);
  };

  return (
    <>
    <Box display='flex' alignItems='center' gap={1}><LabelIcon color="disabled"/>User Management</Box>
      <Grid container>
        <Grid item xs={12}>
          <Box display="flex" alignItems="center" gap={2}>
            <TextFieldStyled
              label="UserId or Title"
              id="filled-size-small"
              value={valueFilter}
              onChange={changeValue}
              variant="outlined"
              size="small"
              InputLabelProps={{ shrink: true }}
            />
          </Box>
        </Grid>
        <TableContainer component={Paper} style={{ marginRight: "20px" }}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead style={{ backgroundColor: "#fafafa" }}>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>UserId</TableCell>
                <TableCell>Title</TableCell>
                <TableCell align="right">Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data?.length > 0 ? (
                data
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  ?.map((row) => <UserRow user={row} />)
              ) : (
                <TableRow>
                  <TableCell colSpan={4}>
                    <Box
                      display="flex"
                      justifyContent="center"
                      alignItems="center"
                      flexDirection="column"
                      textAlign="center"
                      mt={5}
                      mb={5}
                    >
                      <DraftsRoundedIcon color="disabled" fontSize="large" />
                      <Typography variant="body1" color="textSecondary">
                        No data to display
                      </Typography>
                    </Box>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component={"div"}
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Grid>
    </>
  );
};

export default React.memo(Posts);

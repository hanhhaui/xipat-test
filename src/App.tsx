import React from "react";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import "./App.css";
import Layout, { routes } from "./modules/layout";
import { Grid } from "@mui/material";

function App() {
  return (
    <BrowserRouter>
      <Grid container spacing={2} m={2}>
        <Grid item xs={1}>
          <Layout />
        </Grid>
        <Grid item xs={11}>
          <Routes>
            <Route
              path="*"
              element={<Navigate to="/dashboard" replace />}
            />
            {routes?.map((item) => {
              return (
                <Route
                  key={item?.path}
                  path={item?.path}
                  element={item?.element}
                />
              );
            })}
          </Routes>
        </Grid>
      </Grid>
    </BrowserRouter>
  );
}

export default App;
